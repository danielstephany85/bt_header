'use strict';

$.fn.swipeRight = function(cb){;
    return this.each(function(i, el){
        var self = this;
        swipe(self, 'right', cb);
    });
}
$.fn.swipeLeft = function(cb){
    return this.each(function(i, el){
        var self = this;
        swipe(self, 'left', cb);
    });
}

function swipe(el, direction, cb){
    var element = el;
    var swipebool;
        var startP;
        var startX;
        var startY;
        var directionModifier;
        switch(direction){
            case 'right':
                directionModifier = -1;
                break;
            case 'left':
                directionModifier = 1;
                break;
            default: directionModifier = 1;;
        }
        element.addEventListener("touchstart", function(e){
            swipebool = true;
            startX = e.changedTouches[0].clientX;
            startY = e.changedTouches[0].clientY;
        }, false); 
        element.addEventListener("touchmove", function(e){
            var toucheX = e.changedTouches[0].clientX;
            var toucheY = e.changedTouches[0].clientY;
            var distanceX = startX - toucheX;
            var distanceY = startY - toucheY;
            if(distanceY < 0 ){distanceY *= -1;}
            if (distanceX <= 150 && swipebool && distanceY < (distanceX * directionModifier)){
                swipebool = false;
                return cb.call(this, el);
            }
        }, false); 
}

$('.nav-drawer-toggle').on('click', function(){
	var $body = $("body");
	if($body.hasClass('main-nav-drawer-active')) {
		$body.removeClass('main-nav-drawer-active');
		$('.main-nav-overlay').removeClass('active');
		setTimeout(function(){
			$('.main-nav-overlay').remove();
		},250);
	} else {
		$body.addClass('main-nav-drawer-active');
		$('.site-constraint').append("<div class='main-nav-overlay'></div>");
		setTimeout(function(){
			$('.main-nav-overlay').addClass('active');
		},50);
	}
});

$(document).on('click', '.main-nav-overlay', function(e){
	var $this = $(this);
	$this.removeClass('active');
	setTimeout(function(){
			$this.remove();
		},250);
	$('.main-nav-drawer-active').removeClass('main-nav-drawer-active');
});

$('.main-nav-drawer').swipeRight(function(){
	$('.main-nav-drawer-active').removeClass('main-nav-drawer-active');
	$('.main-nav-overlay').removeClass('active');
		setTimeout(function(){
			$('.main-nav-overlay').remove();
		},250);
});
//# sourceMappingURL=app.js.map
