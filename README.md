# bt_header #



### setup ###
* add a div with the class **site-constraint** to your project. The site constraint should be a child of the body element and wrap all content.
* Include the header html inside the site-constraint.
* add the javascript inside app.js to your project
* add the css in main.css to your css file or grab the header folder that contains the sass and put it in your scss folder and import __stage.scss to get all of the scss styles.

###optional###
* some of the styling relies on google fonts and fontawsome, if you wish to use these styles include the calls to the cdn that you will find in the head of index.html located in the **working_example** folder. 
* if you do not use fontawsome delete the <i> elements in the header

### dependencies ###

* jQuery